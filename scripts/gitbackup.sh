#!/bin/ksh
#
#	THIS IS A KORN SHELL SCRIPT
#
#	Copyright (c) - Enterprise Consultancy Services (ECS) Limited
#
#	Name    : gitbackup.sh 
#	Release : see $VER below
#
#	Purpose :	To clone all bitbucket repos zip them up and arhive them
#
#	Author  :	Tim Heeley - ECS Ltd
#
#	Date    :	11/01/2018
#
#	Description :	Get a list of all theeley repositories , clone each one them zip them up
#			and upload them to ftp.enterprise-cs.co.uk
#
#	Calling Seq :	gitbackup.sh
#
#	Amendments  :	
#
#	EOA - End of Amendments
##############################
PROG=$( basename --suffix=".sh" $0 )
exec 2> /tmp/${PROG}.trc
set -x
##############################
## Standard Version display ##
##############################
export VER="1.0.2" 
showVersion()
{
        echo $VER
}

getNextArcno()
{
	ftpecs.sh ecs get bitbucket/nextno.txt nextno.txt >/dev/null 2>&1
	R=$?

	if [[ $R -eq 0 && -f nextno.txt ]]
	then
		head -1 nextno.txt 
	else
		echo 0
	fi
}
##############################
## Remove old back ups
## Keeping a rolling 7 copies
##############################

ftpTidyArchive()
{

	ftpecs.sh ecs list bitbucket > $TMP1

	grep "^bbarchive" $TMP1 > $TMP2
	x=$( cat $TMP2 | wc -l )
	x=$(( $x - 6 ))
	head -$x $TMP2 > $archivelist

	if [[ $x -gt 0 ]]
	then	
		while read line
		do
			Echo "Removing $line"	
			ftpecs.sh ecs delete "bitbucket/${line}"	
			R=$?
			if [[ $R -eq 0 ]]
			then
				Echo "$line removed"
			else
				Echo "$line removal failed!"
			fi
	
		done < $archivelist
	fi

}


SCRIPT="/usr/local/script"
BIN="/usr/local/bin"
TMP=/tmp/backup$$
WORK=$TMP/work
TMP1=${TMP}/work1
TMP2=${TMP}/work2
TMP3=${TMP}/work3
archivelist=${TMP}/archivelist.txt
mkdir -m777 $TMP
mkdir -m777 $WORK
BBbase="git@bitbucket.org:"
HTTPurl="https://theeley:Abacab!23@bitbucket.org/"
BBsuffix=".git"
RUNLOG=${TMP}/${PROG}.log
EMAILER=/usr/local/script/email.sh
EMADMIN="theeley"
#
#################################
## MAIN SCRIPT BEGINS          ##
#################################

#################################
## Display Version if asked    ##
## to do so                    ##
#################################
	if [[ $# -eq 1 ]]
	then
        	if [[ "$1" = "--version" ]]
        	then
                	echo "Version $(showVersion)"
                	exit 0
        	fi
	fi



	cd $WORK
	. ecslib.sh

	. gitBB.sh

	Echo "$0 begins"

	# populateRepoList

	###########################
	## Keep a rolling archive
	## Remove the oldest
	###########################
	
	ftpTidyArchive

	repocount=$( cat $BBREPOLIST | wc -l )

	rm -f /tmp/gitreps

	chkcount=0
	while read indx source repo
	do

		OWNER=$( echo $source | cut -f2 -d'/' )


		#git clone https://theeley:Abacab!23@bitbucket.org/theeley/${repo}${BBsuffix}
		#git clone ${HTTPurl}${OWNER}/${repo}${BBsuffix}
		git clone ${BBbase}${OWNER}/${repo}${BBsuffix}
		R=$?

		if [[ $R -ne 0 ]]
		then
			Echo "Error $R for $repo"
			break
		fi

		chkcount=$( expr $chkcount + 1 )

	done < ${BBREPOLIST}

	if [[ $chkcount -ne $repocount ]]
	then
		Echo "Repository count mismatch"
		Echo "Repo count = $repocount"
		Echo "Porecessed count = $chkcount"
	else
		Echo "All repos processed"
	fi

	ARCNO=$( getNextArcno )

	cd $TMP
	while [[ $R -eq 0 ]]
	do
		doCommand "tar cf bbarchive.${ARCNO} work*"
		R=$?
		
		doCommand "zip bbarchive.${ARCNO}.zip bbarchive.${ARCNO}"
		R=$?
	
		doCommand "ftpecs.sh ecs put bbarchive.${ARCNO}.zip bitbucket/bbarchive.${ARCNO}.zip "
		R=$?
		Echo "bbarchive.${ARCNO}.zip archive to ftp.enterprise-cs.co.uk\bitbucket"


		ARCNO=$( expr $ARCNO + 1 )
		echo $ARCNO > ${WORK}/nextno.txt
		doCommand "ftpecs.sh ecs put ${WORK}/nextno.txt bitbucket/nextno.txt "
		R=$?	


		break
	done


	if [[ $R -eq 0 ]]
	then
		Echo "${PROG}: Complete"
	else
		Echo "${PROG}: Failed"
	fi
	  

	$EMAILER $EMADMIN $RUNLOG 
	
	rm -rf $TMP
